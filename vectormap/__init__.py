import logging
import random
from dataclasses import dataclass
from itertools import chain

log = logging.getLogger(__name__)


def iter_fields(obj):
    for field in obj.__dataclass_fields__.keys():
        yield getattr(obj, field)


def data(cls):
    """Decorator for creating namedtuple-like dataclasses."""
    data_cls = dataclass(cls, unsafe_hash=True)
    data_cls.__iter__ = iter_fields
    return data_cls


@data
class Position:
    x: int
    y: int

    def __add__(self, direction):
        return Position(self.x + direction.dx, self.y + direction.dy)


@data
class Direction:
    dx: int
    dy: int

    @property
    def cw(self):
        return Direction(self.dy, -self.dx)

    @property
    def ccw(self):
        return Direction(-self.dy, self.dx)

    @property
    def reverse(self):
        return Direction(-self.dx, -self.dy)

    def next_directions(self):
        yield self.ccw
        yield self
        yield self.cw


Direction.RIGHT = Direction(1, 0)
Direction.DOWN = Direction(0, 1)
Direction.LEFT = Direction(-1, 0)
Direction.UP = Direction(0, -1)


class Path(list):
    def simplified(self):
        result = Path()
        positions = iter(self)
        segment_start = next(positions)
        segment_end = next(positions)
        try:
            while True:
                continuation = next(positions)
                # log.debug('continuation: %r', continuation)
                if (segment_start.x == segment_end.x == continuation.x) or (
                    segment_start.y == segment_end.y == continuation.y
                ):

                    segment_end = continuation
                else:
                    result.append(segment_start)
                    segment_start = segment_end
                    segment_end = continuation
        except StopIteration:
            result.append(segment_start)
            result.append(segment_end)

        # log.debug('result: %r', result)
        # log.debug('list(result): %r', list(result))
        return tuple(result)

    def __repr__(self):
        if len(self) > 2:
            return f"[{self[0]!r}, ..., {self[-1]!r}]"
        else:
            return super().__repr__()


@data
class Border:
    regions: list
    path: Path


@data
class Crossing:
    start: Position
    end: Position

    @staticmethod
    def from_xy(*, x0, x1, y0, y1):
        return Crossing(Position(x0, y0), Position(x1, y1))


@data
class Extents:
    x0: int
    x1: int
    y0: int
    y1: int

    def union(self, other):
        return Extents(
            x0=min(self.x0, other.x0),
            x1=max(self.x1, other.x1),
            y0=min(self.y0, other.y0),
            y1=max(self.y1, other.y1),
        )

    @property
    def center(self):
        return Position((self.x0 + self.x1) / 2, (self.y0 + self.y1) / 2)

    @property
    def rectangle(self):
        return (self.x0, self.y0, self.x1 - self.x0, self.y1 - self.y0)

    @classmethod
    def from_path(cls, path):
        xs = [p.x for p in path]
        ys = [p.y for p in path]
        return cls(x0=min(xs), x1=max(xs), y0=min(ys), y1=max(ys))

    @classmethod
    def from_region(cls, region):
        border_extents = [cls.from_path(b.path) for b in region.borders()]
        extent = border_extents[0]
        for next_extent in border_extents[1:]:
            extent = extent.union(next_extent)
        return extent


class Region:
    def __init__(self, colour=None):
        self.colour = colour
        self.loops = []
        self.loop_closed = True

    def add_border(self, border, *, reverse):
        if self.loop_closed:
            self.loop_closed = False
            self.loops.append([])

        self.loops[-1].append((border, reverse))

        if self.last_loop_start_position() == self.last_path_position():
            self.loop_closed = True

    def closed(self):
        return self.loop_closed

    def last_loop_start_position(self):
        border, reverse = self.loops[-1][0]
        return border.path[-1 if reverse else 0]

    def last_path_position(self):
        border, reverse = self.loops[-1][-1]
        return border.path[0 if reverse else -1]

    def __repr__(self):
        return f"Region({self.colour}, loops=...)"

    def borders(self):
        for loop in self.loops:
            for border, _ in loop:
                yield border


DEFAULT_COUNTRY_BORDERING = [
    # Used for picking color shading variations. Has extra borders
    # added to pick more pleasing configurations.
    [4, 5, 7, 8],
    [3, 4, 11],
    [3, 10, 14],
    [1, 2, 10, 16, 19],
    [0, 1, 7, 11, 8],
    [0, 6, 12],
    [5, 12, 13, 18],
    [0, 4, 11, 8, 15, 17],
    [0, 7, 15, 4],
    [13, 14, 21, 23],
    [2, 3, 14, 16],
    [1, 4, 7, 17],
    [5, 6, 18, 20],
    [6, 9, 18, 21, 22, 25],
    [2, 9, 10, 16, 23],
    [7, 8, 17, 24],
    [3, 10, 14, 19, 23],
    [7, 11, 15, 24],
    [6, 12, 13, 20, 22],
    [3, 16],
    [12, 18, 22, 26],
    [9, 13, 23, 25, 28],
    [13, 18, 20, 25, 26, 27],
    [9, 14, 16, 21, 28, 29],
    [15, 17, 30],
    [13, 21, 22, 27, 28],
    [20, 22, 27, 32, 33, 34],
    [22, 25, 26, 33],
    [21, 23, 25, 29],
    [23, 28, 35],
    [24, 31, 32, 38],
    [30, 32, 38],
    [26, 30, 31, 38],
    [26, 27, 34, 37, 39],
    [26, 33, 39],
    [29, 36, 40],
    [35, 40, 41],
    [33, 39],
    [31, 32, 30],
    [33, 34, 37],
    [35, 36, 41],
    [36, 40],
]

COLOUR_VARIATIONS = set(range(4))


class Map:
    def __init__(self, *, borders, regions, width, height, crossings=None):
        self.borders = borders
        self.regions = regions
        self.width = width
        self.height = height
        self.crossings = crossings or []

        for n, border in enumerate(self.borders):
            border.index = n

        for n, region in enumerate(self.regions):
            region.index = n

    def assign_country_colour_variations(self, start_index=0):
        """Assign different colour variation for bordering countries.

        Only works for standard region borderings."""

        if start_index == len(self.regions):
            return True

        region = self.regions[start_index]
        if hasattr(region, "colour_variation"):
            return self.assign_country_colour_variations(start_index + 1)
        neighbours = {
            getattr(self.regions[n], "colour_variation", None)
            for n in DEFAULT_COUNTRY_BORDERING[start_index]
        }
        for v in COLOUR_VARIATIONS.difference(neighbours):
            region.colour_variation = v
            if self.assign_country_colour_variations(start_index + 1):
                return True
        return False

    def to_json(self):
        def border_json(border):
            result = []
            for point in border.path:
                result.extend(point)
            return result

        def region_json(region):
            return [
                (border.index << 1) | reverse
                for border, reverse in chain(*region.loops)
            ]

        result = {
            "borders": [border_json(b) for b in self.borders],
            "regions": [region_json(r) for r in self.regions],
            "size": [self.width, self.height],
        }
        return result

    @classmethod
    def from_json(cls, data, noise=0):
        def border_from_json(border_data):
            position_data = iter(border_data)

            def pos(x, y):
                return Position(
                    x + random.triangular(-noise, noise),
                    y + random.triangular(-noise, noise),
                )

            path = Path((pos(x, y) for x, y in zip(position_data, position_data)))
            return Border(regions=[], path=path)

        def region_from_json(region_data, *, borders):
            region = Region()
            for border_data in region_data:
                border = borders[border_data >> 1]
                reverse = border_data & 1
                region.add_border(border, reverse=reverse)
                border.regions.append(region)
            return region

        borders = [border_from_json(d) for d in data["borders"]]
        regions = [region_from_json(d, borders=borders) for d in data["regions"]]
        width, height = data["size"]

        return Map(borders=borders, regions=regions, width=width, height=height)
