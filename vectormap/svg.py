import logging
import os.path

import lxml.etree as etree

from vectormap import Crossing, Extents
from vectormap.trace import trace_image

COUNTRY_NAMES = [
    "Grönland",
    "Alaska",
    "Jakutsk",
    "Kamchatka",
    "Nunavut",
    "Island",
    "Skandinavien",
    "Ontario",
    "Quebec",
    "Ural",
    "Irkutsk",
    "Columbia",
    "Albion",
    "Ukraina",
    "Sibirien",
    "Cherokee",
    "Mongoliet",
    "Sioux",
    "Preussen",
    "Japan",
    "Iberia",
    "Khazakstan",
    "Bysans",
    "Taiwan",
    "Mexico",
    "Arabien",
    "Songhai",
    "Egypten",
    "Hindustan",
    "Siam",
    "Venezuela",
    "Peru",
    "Brasilien",
    "Östafrika",
    "Kongo",
    "Indonesien",
    "Papua",
    "Madagaskar",
    "Argentina",
    "Zimbabwe",
    "Sahul",
    "Victoria",
]


SVG_NS = "http://www.w3.org/2000/svg"
XLINK_NS = "http://www.w3.org/1999/xlink"
NS = {"svg": SVG_NS, "xlink": XLINK_NS}
NS_MAP = {None: SVG_NS, "xlink": XLINK_NS}

log = logging.getLogger(__name__)


def write_svg(vector_map):
    svg = etree.Element("svg")
    svg.set("width", str(vector_map.width))
    svg.set("height", str(vector_map.height))

    for region in vector_map.regions:
        colour = region.colour
        loops = region.loops
        path_element = etree.SubElement(svg, "path")
        path_element.set("fill", f"rgb{colour!r}")
        path_element.set("fill-rule", "evenodd")
        d = []
        for borders in loops:
            d1 = []
            for border, reverse in borders:
                if reverse:
                    points = list(reversed(border.path))
                else:
                    points = list(border.path)
                x, y = points.pop()
                for x, y in points:
                    if not d1:
                        d1.append(f"M {x},{y}")
                    else:
                        d1.append(f"L {x},{y}")
            d.extend(d1)
        path_element.set("d", " ".join(d))

    print(etree.tostring(svg, pretty_print=True).decode("utf-8"))


def write_metadata_svg(*, vector_map, bitmap_path):
    svg = etree.Element(f"{{{SVG_NS}}}svg", nsmap=NS_MAP)
    svg.set("width", str(vector_map.width))
    svg.set("height", str(vector_map.height))

    image = etree.SubElement(svg, "image")
    image.set(f"{{{XLINK_NS}}}href", bitmap_path)
    image.set("width", str(vector_map.width))
    image.set("height", str(vector_map.height))

    for n, region in enumerate(vector_map.regions):
        extents = Extents.from_region(region)
        circle = etree.SubElement(svg, "circle")
        index = getattr(region, "index", n)
        circle.set("id", str(index))
        circle.set("cx", str(getattr(region, "cx", extents.center.x)))
        circle.set("cy", str(getattr(region, "cy", extents.center.y)))
        circle.set("r", "3")
        text = etree.SubElement(svg, "text")
        text.set("x", str(getattr(region, "text_x", extents.center.x)))
        text.set("y", str(getattr(region, "text_y", extents.center.y)))
        text.set("id", f"t_{index}")
        text.set("text-anchor", "middle")
        text.set("font-size", str(getattr(region, "text_size", 4)))
        text.text = COUNTRY_NAMES[index]

    for crossing in vector_map.crossings:
        path = etree.SubElement(svg, "path")
        path.set(
            "d",
            f"M {crossing.start.x},{crossing.start.y} {crossing.end.x},{crossing.end.y}",
        )

    print(etree.tostring(svg, pretty_print=True).decode("utf-8"))


def parse_length(text):
    if text.endswith("px"):
        return float(text[:-2])
    else:
        return float(text)


def read_metadata_svg(svg_path):
    with open(svg_path, "r") as svg_file:
        svg = etree.parse(svg_file)
    image_rel_path = svg.xpath("string(svg:image/@xlink:href)", namespaces=NS)
    image_path = os.path.join(os.path.dirname(svg_path), image_rel_path)
    vector_map, colour_map, image = trace_image(path=image_path)

    circles = svg.xpath("svg:circle", namespaces=NS)
    regions = {}
    for circle in circles:
        cx = float(circle.xpath("string(@cx)"))
        cy = float(circle.xpath("string(@cy)"))
        colour = image.getpixel((cx, cy))
        n = int(circle.xpath("string(@id)"))
        try:
            region = colour_map[colour]
        except KeyError as err:
            raise KeyError(f"circle #{n} @{cx},{cy} is over an unknown colour") from err
        region.cx = cx
        region.cy = cy
        region.index = n
        regions[n] = region

    texts = svg.xpath("svg:text", namespaces=NS)
    for text in texts:
        text_x = float(text.xpath("string(@x)"))
        text_y = float(text.xpath("string(@y)"))
        n = int(text.xpath("string(@id)")[2:])
        region = regions[n]
        region.text_x = text_x
        region.text_y = text_y
        font_size = text.xpath("string(@font-size)")
        if font_size:
            text_size = parse_length(font_size)
        style = text.xpath("string(@style)")
        styles = style.split(";")
        for style in (s for s in styles if s):
            prop, value = style.split(":")
            if prop == "font-size":
                text_size = parse_length(value)
        region.text_size = text_size
        region.text = text.text

    paths = svg.xpath("svg:path", namespaces=NS)
    for path in paths:
        d = path.xpath("string(@d)")
        log.debug(d)
        try:
            m, start, *rest = d.split()
            if m == "M":
                relative = False
            elif m == "m":
                relative = True
            else:
                raise ValueError()
            start_x, start_y = [int(i) for i in start.split(",")]
            if "," in rest[0]:
                end_x, end_y = [int(i) for i in rest[0].split(",")]
                if relative:
                    end_x += start_x
                    end_y += start_y
            elif rest[0] == "h":
                end_x = start_x + int(rest[1])
                end_y = start_y
            elif rest[0] == "H":
                end_x = int(rest[1])
                end_y = start_y
            elif rest[0] == "v":
                end_x = start_x
                end_y = start_y + int(rest[1])
            elif rest[0] == "V":
                end_x = start_x
                end_y = int(rest[1])
            crossing = Crossing.from_xy(x0=start_x, y0=start_y, x1=end_x, y1=end_y)
            vector_map.crossings.append(crossing)
        except ValueError:
            log.exception("Exception while parsing path description")
            raise

    sorted_regions = [regions[n] for n in range(len(regions))]
    vector_map.regions = sorted_regions
    vector_map.assign_country_colour_variations()

    return vector_map, image_path
