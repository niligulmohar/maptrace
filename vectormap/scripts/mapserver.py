import io
import logging
import os

from aiohttp import web, web_exceptions

from vectormap.draw import draw_map, RegionStatus
from vectormap.svg import read_metadata_svg


log = logging.getLogger(__name__)


def parse_data(data):
    parts = data.split(",")
    region_data = []
    config = {}

    for part in parts:
        if not part:
            continue
        elif part[0].isnumeric():
            region_data.append(RegionStatus(int(part)))
        else:
            config[part[0]] = part[1:]

    return region_data, config


async def map_handler(request):
    try:
        data = request.match_info.get("data")
        if data == "random":
            cache_time = 0
            region_data = [RegionStatus.random() for _ in request.app["map"].regions]
            config = {}
        elif data is None:
            cache_time = 300
            region_data = None
            config = {}
        else:
            cache_time = 300
            region_data, config = parse_data(data)
    except Exception as exc:
        log.exception("Data parsing failed")
        raise web_exceptions.HTTPBadRequest() from exc

    width = 720
    size = request.match_info.get("size")
    if size is None:
        pass
    elif size == "L":
        width = 978
    else:
        raise web_exceptions.HTTPBadRequest()

    body = io.BytesIO()
    draw_map(
        request.app["map"],
        region_data=region_data,
        output_file=body,
        target_width=width,
        config=config,
    )
    return web.Response(
        body=body.getbuffer(),
        content_type="image/png",
        headers={"Cache-Control": f"max-age={cache_time}"},
    )


def get_application():
    app = web.Application()
    app.router.add_get("/v0", map_handler)
    app.router.add_get("/v0/", map_handler)
    app.router.add_get("/v0/{data}", map_handler)
    app.router.add_get("/v0/{size}/", map_handler)
    app.router.add_get("/v0/{size}/{data}", map_handler)

    vector_map, _ = read_metadata_svg("data/map2.svg")

    app["map"] = vector_map

    return app


def main():
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("PIL").setLevel(logging.WARNING)
    logging.getLogger("vectormap.trace").setLevel(logging.WARNING)
    logging.getLogger("vectormap.svg").setLevel(logging.WARNING)
    app = get_application()
    web.run_app(
        app,
        host=os.environ.get("HOST", "127.0.0.1"),
        port=os.environ.get("PORT", "8888"),
    )
