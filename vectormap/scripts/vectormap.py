import json as json_module
import logging
import sys

import click

from vectormap.svg import read_metadata_svg, write_svg, write_metadata_svg
from vectormap.trace import trace_image

log = logging.getLogger(__name__)


@click.command()
@click.option("--json/--no-json")
@click.option("--svg/--no-svg")
@click.option("--metadata-svg/--no-metadata-svg")
@click.option("-v", "--verbose", count=True)
@click.argument("input_path")
def main(input_path, json, svg, metadata_svg, verbose):
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    if input_path.endswith("svg"):
        vector_map, image_path = read_metadata_svg(input_path)
    else:
        vector_map, _, _ = trace_image(path=input_path)
        image_path = input_path

    if json:
        json_module.dump(vector_map.to_json(), sys.stdout)
    if svg:
        write_svg(vector_map=vector_map)
    if metadata_svg:
        write_metadata_svg(vector_map=vector_map, bitmap_path=image_path)
