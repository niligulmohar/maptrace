import logging

import click

from vectormap.draw import draw_map, RegionStatus
from vectormap.svg import read_metadata_svg
from vectormap.trace import trace_image


@click.command()
@click.option("-v", "--verbose", count=True)
@click.argument("input_path", nargs=1)
@click.argument("data", nargs=-1)
def main(input_path, verbose, data):
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    if input_path.endswith("svg"):
        vector_map, _ = read_metadata_svg(input_path)
    else:
        vector_map, _, _ = trace_image(path=input_path)

    region_data = [RegionStatus(int(n)) for n in data]

    with open("out.png", "wb") as png_file:
        draw_map(vector_map, region_data=region_data, output_file=png_file)
