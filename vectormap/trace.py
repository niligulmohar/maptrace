import collections
import logging

from PIL import Image

from . import Border, Direction, Map, Path, Position, Region

log = logging.getLogger(__name__)

OCEAN_COLOUR = 0


class PixelEdge(collections.namedtuple("PixelEdge", ["direction", "position"])):
    @property
    def positive_edge(self):
        direction, pos = self
        if self.direction == Direction.LEFT:
            pos += direction
            direction = Direction.RIGHT
        elif direction == Direction.UP:
            pos += direction
            direction = Direction.DOWN
        return PixelEdge(direction, pos)

    @property
    def reverse(self):
        return PixelEdge(self.direction.reverse, self.position + self.direction)


def get_pixel_edges(image):
    width, height = image.size
    pixel_data = image.load()

    pixel_edges = {}

    def pixel(x, y):
        if 0 <= x < width and 0 <= y < height:
            return pixel_data[x, y]
        else:
            return OCEAN_COLOUR

    for y in range(0, height + 1):
        for x in range(0, width + 1):
            pos = Position(x, y)
            pixels = sorted([pixel(x + dx, y) for dx in (-1, 0)])
            if pixels[0] != pixels[1]:
                edge = PixelEdge(Direction.DOWN, pos)
                pixel_edges[edge] = pixels
            pixels = sorted([pixel(x, y + dy) for dy in (-1, 0)])
            if pixels[0] != pixels[1]:
                edge = PixelEdge(Direction.RIGHT, pos)
                pixel_edges[edge] = pixels

    return pixel_edges


def get_borders(pixel_edges):
    borders = []

    while len(pixel_edges):
        edge, pixels = pixel_edges.popitem()
        edge_starts = [edge, edge.reverse]

        paths = []
        for edge in edge_starts:
            direction, pos = edge
            path = [pos]
            while direction is not None:
                pos += direction
                path.append(pos)
                for next_direction in direction.next_directions():
                    next_edge = PixelEdge(next_direction, pos)
                    next_pixels = pixel_edges.get(next_edge.positive_edge)
                    if pixels == next_pixels:
                        direction = next_direction
                        pixel_edges.pop(next_edge.positive_edge)
                        break
                else:
                    direction = None
            paths.append(path)
        path = Path(Path(reversed(paths[1][2:])) + paths[0]).simplified()
        borders.append(Border(pixels, path))

    log.debug("%d borders", len(borders))

    return borders


def get_regions(borders):
    region_colours = set()

    for border in borders:
        region_colours.update(border.regions)

    region_colours.remove(OCEAN_COLOUR)
    region_colour_map = {}

    regions = []
    for region_colour in region_colours:
        log.debug("region_colour: %r", region_colour)
        region = Region(region_colour)
        region_colour_map[region_colour] = region
        region_borders = [b for b in borders if region_colour in b.regions]
        log.debug("region %r, region_borders: %r", region, region_borders)
        while region_borders:
            while region.closed() and region_borders:
                region.add_border(region_borders.pop(), reverse=False)

            while not region.closed() and region_borders:
                last_pos = region.last_path_position()
                log.debug("last_pos: %r", last_pos)
                next_border = None
                for border in region_borders:
                    region_colours, path = border
                    log.debug("path: %r", path)
                    if path[0] == last_pos:
                        next_border = border
                        reverse = False
                        break
                    elif path[-1] == last_pos:
                        next_border = border
                        reverse = True
                        break
                else:
                    raise RuntimeError("Could not close border loop")
                if next_border is not None:
                    region_borders.remove(next_border)
                    region.add_border(next_border, reverse=reverse)
        regions.append(region)

    for border in borders:
        border.regions = [
            region_colour_map[c] for c in border.regions if c != OCEAN_COLOUR
        ]

    log.debug("*****")
    log.debug("regions: \n%s", "\n".join(repr(r) for r in regions))
    log.debug("*****")

    return regions, region_colour_map


def trace_image(*, path):
    image = Image.open(path)
    width, height = image.size

    pixel_edges = get_pixel_edges(image)

    borders = get_borders(pixel_edges)

    regions, region_colour_map = get_regions(borders)

    vector_map = Map(borders=borders, regions=regions, width=width, height=height)

    return vector_map, region_colour_map, image
