import colorsys
import math
import random

import cairo

from vectormap import Position


def draw_region_path(region, *, context):
    for loop in region.loops:
        first = True
        for border, reverse in loop:
            points = reversed(border.path) if reverse else border.path
            for x, y in points:
                if first:
                    context.move_to(x, y)
                    first = False
                else:
                    context.line_to(x, y)
    context.close_path()


def draw_border_path(border, *, context):
    first = True
    points = border.path
    for x, y in points:
        if first:
            context.move_to(x, y)
            first = False
        else:
            context.line_to(x, y)


class Colour:
    def __init__(self, r, g, b, *, dh=0, dl=0, ds=0):
        self.r = r
        self.g = g
        self.b = b
        self.dh = dh
        self.dl = dl
        self.ds = ds

    @property
    def rgb(self):
        return self.r, self.g, self.b

    def rgb_variation(self, variation=0, *, dl=0, ds=0):
        h, l, s = colorsys.rgb_to_hls(*self.rgb)
        h += self.dh * (3 - variation)
        l += self.dl * (3 - variation) + dl
        s += self.ds * (3 - variation) + ds
        return colorsys.hls_to_rgb(h, l, s)


WHITE = Colour(1, 1, 1)
BLACK = Colour(0, 0, 0)

SEA_0 = Colour(0.7, 0.85, 0.95)
SEA_1 = Colour(0.55, 0.7, 0.8)

DEFAULT_COLOUR = Colour(0.8, 0.8, 0.8, dl=-0.02)
CROSSING_COLOUR = Colour(0, 0, 0.5)
BLUE = Colour(0.2, 0.2, 1, dl=-0.04, ds=-0.05)
RED = Colour(0.65, 0, 0, dl=0.02)
YELLOW = Colour(1, 0.95, 0, dh=-0.01, dl=-0.01)
GREEN = Colour(0.1, 0.7, 0, dl=-0.02, ds=-0.06)
PURPLE = Colour(0.8, 0, 0.7, dl=-0.02, dh=-0.02)
BROWN = Colour(0.6, 0.35, 0.3, dl=-0.03, dh=-0.02)

CONTINENTS = (
    ("Nordamerika", 5, (0, 1, 4, 7, 8, 11, 15, 17, 24), YELLOW),
    ("Europa", 5, (5, 6, 12, 13, 18, 20, 22), BLUE),
    ("Asien", 7, (2, 3, 9, 10, 14, 16, 19, 21, 23, 25, 28, 29), GREEN),
    ("Sydamerika", 2, (30, 31, 32, 38), RED),
    ("Afrika", 3, (26, 27, 33, 34, 37, 39), BROWN),
    ("Oceanien", 2, (35, 36, 40, 41), PURPLE),
)

REGION_COLOURS = {}

for _, _, regions, colour in CONTINENTS:
    for region in regions:
        REGION_COLOURS[region] = colour


class RegionStatus:
    owner_colours = (DEFAULT_COLOUR, BLUE, RED, YELLOW, GREEN, PURPLE, BROWN)
    complement_colours = (BLACK, WHITE, WHITE, BLACK, WHITE, BLACK, BLACK)

    def __init__(self, data):
        self.owner = data % 7
        self._armies = data // 7

    @property
    def armies(self):
        if self._armies:
            return self._armies
        else:
            return "?"

    @property
    def colour(self):
        return self.owner_colours[self.owner]

    @property
    def complement_colour(self):
        return self.complement_colours[self.owner]

    @classmethod
    def random(cls):
        return cls(random.randrange(1, 5) + 7 * random.randrange(1, 10))


def draw_map(vector_map, *, region_data, config, output_file, target_width=360):
    width, height = vector_map.width, vector_map.height
    aspect = width / height
    target_height = int(target_width / aspect)
    scale = target_width / width

    def region_status(n):
        try:
            return region_data[n]
        except IndexError:
            return RegionStatus(0)

    with cairo.ImageSurface(cairo.Format.RGB24, target_width, target_height) as surface:
        context = cairo.Context(surface)
        context.scale(scale, scale)

        context.rectangle(0, 0, width, height)
        gradient = cairo.LinearGradient(0, 0, 0, height)
        gradient.add_color_stop_rgb(0, 0.7, 0.85, 0.95)
        gradient.add_color_stop_rgb(1, 0.55, 0.7, 0.8)
        context.set_source(gradient)
        context.fill()

        context.set_line_join(cairo.LineJoin.ROUND)
        context.set_line_cap(cairo.LineCap.ROUND)
        context.set_antialias(cairo.Antialias.BEST)
        context.set_fill_rule(cairo.FillRule.EVEN_ODD)

        context.set_source_rgb(*CROSSING_COLOUR.rgb)
        context.set_line_width(1)
        context.set_dash((0, 1.5))
        for crossing in vector_map.crossings:
            context.move_to(crossing.start.x, crossing.start.y)
            context.line_to(crossing.end.x, crossing.end.y)
            context.stroke()

        context.set_dash(())
        for i, region in enumerate(vector_map.regions):
            draw_region_path(region, context=context)
            if region_data:
                colour = region_status(i).colour
                rgb = colour.rgb_variation(region.colour_variation, ds=-0.32, dl=0.16)
            else:
                colour = REGION_COLOURS[i]
                rgb = colour.rgb_variation(region.colour_variation)
            context.set_source_rgb(*rgb)
            context.fill()

        context.set_source_rgb(*BLACK.rgb)
        for border in vector_map.borders:
            if region_data:
                data = [region_status(r.index).owner for r in border.regions]
            else:
                data = [REGION_COLOURS[r.index] for r in border.regions]

            if len(data) == 1 or data[0] != data[1]:
                width = 0.5
            else:
                width = 0.125
            context.set_line_width(width)
            draw_border_path(border, context=context)
            context.stroke()

        context.set_line_width(0.5)
        context.select_font_face(
            "sans-serif", cairo.FontSlant.NORMAL, cairo.FontWeight.BOLD
        )
        for region in vector_map.regions:
            pos = Position(region.text_x, region.text_y)
            text = region.text
            context.set_font_size(region.text_size * 0.8)
            extents = context.text_extents(text)
            context.move_to(round(pos.x - (extents.width / 2)), pos.y)
            context.text_path(text)
            context.set_source_rgba(*BLACK.rgb, 0.5)
            context.stroke_preserve()
            context.set_source_rgb(*WHITE.rgb)
            context.fill()

        if region_data:
            context.set_font_size(14 / 4.0)
            for n, region in enumerate(vector_map.regions):
                pos = Position(region.cx, region.cy)
                context.arc(pos.x, pos.y, 3, 0, math.pi * 2)

                context.set_source_rgba(*region_status(n).colour.rgb, 1)
                context.fill_preserve()

                context.set_source_rgb(*BLACK.rgb)
                context.stroke()

                text = str(region_status(n).armies)
                extents = context.text_extents(text)
                context.move_to(
                    pos.x - (extents.width / 2), pos.y + (extents.height / 2)
                )
                context.text_path(text)
                context.set_source_rgb(*region_status(n).complement_colour.rgb)
                context.fill()

        if region_data:
            y = vector_map.height - 17
            for n, player in reversed(list(enumerate(("a", "b", "c", "d", "e", "f")))):
                name = config.get(player)
                if name is not None:
                    pos = Position(5, y)
                    context.arc(pos.x, pos.y, 3, 0, math.pi * 2)

                    context.set_source_rgba(*RegionStatus.owner_colours[n + 1].rgb, 1)
                    context.fill_preserve()

                    context.set_source_rgb(*BLACK.rgb)
                    context.stroke()

                    context.move_to(10, y + 1)
                    context.text_path(name)

                    context.set_source_rgba(*WHITE.rgb, 0.5)
                    context.stroke_preserve()
                    context.set_source_rgb(*BLACK.rgb)
                    context.fill()

                    y -= 7
        elif target_width > 400:
            context.select_font_face(
                "sans-serif", cairo.FontSlant.NORMAL, cairo.FontWeight.NORMAL
            )
            context.set_font_size(12 / 4.0)

            y = vector_map.height - 17
            for name, armies, _, colour in reversed(CONTINENTS):
                pos = Position(5, y)
                context.arc(pos.x, pos.y, 3, 0, math.pi * 2)

                context.set_source_rgba(*colour.rgb, 1)
                context.fill_preserve()

                context.set_source_rgb(*BLACK.rgb)
                context.stroke()

                context.move_to(10, y + 1)
                context.text_path(f"{name} +{armies}")

                context.set_source_rgba(*WHITE.rgb, 0.5)
                context.stroke_preserve()
                context.set_source_rgb(*BLACK.rgb)
                context.fill()

                y -= 7

        surface.write_to_png(output_file)
