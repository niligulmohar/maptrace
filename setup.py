from setuptools import find_packages, setup

setup(
    name="vectormap",
    version="0.0.0rc0",
    packages=find_packages(),
    install_requires=["aiohttp", "click", "lxml", "Pillow", "pycairo"],
    entry_points="""
    [console_scripts]
    vectormap = vectormap.scripts.vectormap:main
    drawmap = vectormap.scripts.drawmap:main
    names = vectormap.scripts.names:main
    mapserver = vectormap.scripts.mapserver:main
    """,
)
