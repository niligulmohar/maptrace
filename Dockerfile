FROM python:3.7

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE

ADD source.tar /
WORKDIR /app
RUN id && ls -ld /tmp && pip3 install --no-cache-dir . || ls -lr /tmp
CMD mapserver
