SOURCE_ARCHIVE = source.tar
ARCHIVE_PREFIX = /app

IMAGE_REPO = registry.gitlab.com/niligulmohar/maptrace
IMAGE_FILES += $(SOURCE_ARCHIVE)

include build.mk
